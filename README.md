# Martin Beck Andersen Sleed Challenge

The Sleed Challenge is a web development challenge issued by Sleed as a part of their hiring evaluation process.
Duration was Friday 06/03/2020 to Wednesday 11/03/020. I was set to build a markup for an index page for both desktop
and mobile.

## Preview

![Podio Preview](https://i.imgur.com/8NA2q63.png)

## Copyright

Copyright 2020 Martin Beck Andersen.