<section id="footer-section">
	<div class="container">
		<div class="desktop-show-hide">
			<div class="row d-flex justify-content-center podia-footer-icon">
				<div class="col-md-12">
					<div class="d-flex justify-content-center">
						<img class="img-fluid" src="/img/Podia-Footer-Icon.svg" />
					</div>
				</div>
			</div>
			
			<div class="row desktop-footer-headline">
				<div class="col-md-3 d-inline-block">
					Eπικοινωνία
				</div>
				
				<div class="col-md-3 d-inline-block footer-left-margin">
					Πληροφορίες
				</div>
				
				<div class="col-md-3 d-inline-block footer-left-margin">
					Aκολουθήστε μας
				</div>
			</div>
			
			<div class="row spacer-top-sm">
				<div class="col-md-3 d-inline-block">
					<div class="row">
						<div class="col-md-12">
							<div class="d-inline-block">
								<img class="img-fluid" src="/img/Location-Icon.svg" />
							</div>
							
							<div class="d-inline-block">
								Καλύμνου 36 & Κέννεντυ,
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 left-padding-lg">
							Αγ. Ανάργυροι, Αθήνα
						</div>
					</div>
					
					<div class="row spacer-top-sm">
						<div class="col-md-12">
							<div class="d-inline-block">
								<img class="img-fluid" src="/img/Phone-Icon.svg" />
							</div>
							
							<div class="d-inline-block">
								+30 210 23 81 336
							</div>
						</div>
					</div>
					
					<div class="row spacer-top-sm">
						<div class="col-md-12">
							<div class="d-inline-block">
								<img class="img-fluid" src="/img/Fax-Icon.svg" />
							</div>
							
							<div class="d-inline-block">
								+30 210 23 21 205
							</div>
						</div>
					</div>
					
					<div class="row spacer-top-sm">
						<div class="col-md-12">
							<div class="d-inline-block">
								<img class="img-fluid" src="/img/mail-icon.svg" />
							</div>
							
							<div class="d-inline-block">
								info@podiafootcare.com
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-3 d-inline-block spacer-bottom-sm footer-left-margin">
					<div class="row">
						<div class="col-md-12">
							Για επαγγελματίες
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							Όροι χρήσης
						</div>
					</div>
					
					<div class="row spacer-top-sm">
						<div class="col-md-12">
							Blog
						</div>
					</div>
					
					<div class="row spacer-top-sm">
						<div class="col-md-12">
							Συχνές ερωτήσεις
						</div>
					</div>
					
					<div class="row spacer-top-sm">
						<div class="col-md-12">
							Βρες το κατάλληλο προϊον
						</div>
					</div>
					
					<div class="row spacer-top-sm">
						<div class="col-md-12">
							Επικοινωνία
						</div>
					</div>
				</div>
				
				<div class="col-md-3 d-inline-block footer-left-margin">
					<div class="row">
						<div class="col-md-12">
							<div class="d-inline-block">
								<img class="img-fluid" src="/img/Facebook-Icon.svg" />
							</div>
							
							<div class="d-inline-block">
								Facebook
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<div class="d-inline-block">
								<img class="img-fluid" src="/img/Instagram-Icon.svg" />
							</div>
							
							<div class="d-inline-block">
								Instagram
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<div class="d-inline-block">
								<img class="img-fluid" src="/img/LinkedIn-Icon.svg" />
							</div>
							
							<div class="d-inline-block">
								LinkedIn
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row text-left">
				<div class="col-md-12">
					<div class="copyright-box">
						<p class="copyright">&copy; Copyright <span itemdrop="copyrightYear"><?php echo date('Y');?></span> - All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="mobile-show-hide">
			<div class="row d-flex justify-content-center podia-footer-icon">
				<div class="col-md-12">
					<div class="d-flex justify-content-center">
						<img class="img-fluid" src="/img/Podia-Footer-Icon.svg" />
					</div>
				</div>
			</div>
			
			<div class="row spacer-top-md">
				<div class="col-md-12 custom-collapse-menu-item">
					<a class="footer-menu-button" id="footer-menu-contact-btn" data-toggle="collapse" href="#contactCollapse" role="button"
					aria-expanded="false" aria-controls="contactCollapse">Eπικοινωνία +</a>
				</div>
			</div>
			<div class="footer-contact-menu-collapser collapse multi-collapse spacer-top-sm" id="contactCollapse">
				<div class="row">
					<div class="col-md-12 d-flex">
						<div class="d-inline-block">
							<img class="img-fluid" src="/img/Location-Icon.svg" />
						</div>
						
						<div class="text-left d-inline-block left-margin-md">
							Καλύμνου 36 & Κέννεντυ, <br />
							Αγ. Ανάργυροι, Αθήνα
						</div>
					</div>
				</div>
				
				<div class="row spacer-top-sm">
					<div class="col-md-12">
						<div class="d-inline-block">
							<img class="img-fluid" src="/img/Phone-Icon.svg" />
						</div>
						
						<div class="text-left d-inline-block left-margin-sm">
							+30 210 23 81 336
						</div>
					</div>
				</div>
				
				<div class="row spacer-top-sm">
					<div class="col-md-12">
						<div class="d-inline-block">
							<img class="img-fluid" src="/img/Fax-Icon.svg" />
						</div>
						
						<div class="text-left d-inline-block left-margin-sm">
							+30 210 23 21 205
						</div>
					</div>
				</div>
				
				<div class="row spacer-top-sm">
					<div class="col-md-12">
						<div class="d-inline-block">
							<img class="img-fluid" src="/img/mail-icon.svg" />
						</div>
						
						<div class="text-left d-inline-block left-margin-sm">
							info@podiafootcare.com
						</div>
					</div>
				</div>
			</div>
			
			<div class="row spacer-top-sm">
				<div class="col-md-12 custom-collapse-menu-item">
					<a class="footer-menu-button" id="footer-menu-info-btn" data-toggle="collapse" href="#infoCollapse" role="button"
					aria-expanded="false" aria-controls="infoCollapse">Πληροφορίες +</a>
				</div>
			</div>
			<div class="footer-info-menu-collapser collapse multi-collapse spacer-top-sm" id="infoCollapse">
				<div class="row">
					<div class="col-md-12">
						<a href="#">Για επαγγελματίες</a>
					</div>
				</div>
				
				<div class="row spacer-top-sm">
					<div class="col-md-12">
						<a href="#">Όροι χρήσης</a>
					</div>
				</div>
				
				<div class="row spacer-top-sm">
					<div class="col-md-12">
						<a href="#">Blog</a>
					</div>
				</div>
				
				<div class="row spacer-top-sm">
					<div class="col-md-12">
						<a href="#">Συχνές ερωτήσεις</a>
					</div>
				</div>
				
				<div class="row spacer-top-sm">
					<div class="col-md-12">
						<a href="#">Βρες το κατάλληλο προϊον</a>
					</div>
				</div>
				
				<div class="row spacer-top-sm">
					<div class="col-md-12">
						<a href="#">Επικοινωνία</a>
					</div>
				</div>
			</div>
			
			<div class="footer-socials-container row spacer-top-md spacer-bottom-md">
				<div class="col-12 d-flex">
					<div class="d-inline-block left-padding-md">
						<a href="#"><img class="img-fluid" src="/img/Facebook-Icon.svg" /></a>
					</div>
					
					<div class="d-inline-block left-padding-md">
						<a href="#"><img class="img-fluid" src="/img/Instagram-Icon.svg" /></a>
					</div>
					
					<div class="d-inline-block left-padding-md">
						<a href="#"><img class="img-fluid" src="/img/LinkedIn-Icon.svg" /></a>
					</div>
				</div>
			</div>
			
			<div class="row text-center">
				<div class="col-md-12">
					<div class="copyright-box">
						<p class="copyright">&copy; Copyright <span itemdrop="copyrightYear"><?php echo date('Y');?></span> - All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- JavaScript Libraries -->
<script src="/lib/jquery/jquery.min.js"></script>
<script src="/lib/jquery/jquery-migrate.min.js"></script>
<script src="/lib/popper/popper.min.js"></script>
<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="/lib/easing/easing.min.js"></script>
<script src="/lib/counterup/jquery.waypoints.min.js"></script>
<script src="/lib/counterup/jquery.counterup.js"></script>
<script src="/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="/lib/lightbox/js/lightbox.min.js"></script>
<script src="/lib/typed/typed.min.js"></script>

<!-- Main Javascript File -->
<script src="/js/main.js"></script>

<!-- Custom JavaScript File -->
<script src="/js/custom-js-file.js"></script>
</body>
</html>
<?php ob_end_flush(); ?>