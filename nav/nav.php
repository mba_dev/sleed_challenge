<div itemscope itemtype="http://schema.org/Thing">
	<nav class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">
		<div class="container">
			<a class="navbar-brand" href="#" title="" itemdrop="url"><img class="img-responsive" src="/img/Logo.svg" /></a>
			<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
				aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation" onclick="mobileTap();">
				<span></span>
				<span></span>
				<span></span>
				<div class="mobile-tap-animation"></div>
			</button>
			
			<div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="#" title="" itemdrop="url">Aρχική</a>
					</li>
					<li class="nav-item">
						<a class="nav-link desktop-show-hide" href="#" title="" itemdrop="url">O Δρόμος Μας</a>
						<a class="nav-link mobile-nav-collapser-btn mobile-show-hide" id="mobile-nav-our-way-btn" data-toggle="collapse" href="#our-way-mobile-collapser" role="button"
						aria-expanded="false" aria-controls="our-way-mobile-collapser">
							<div class="row">
								<div class="col-6 text-left">
									O Δρόμος Μας
								</div>
								<div class="col-6 text-right mobile-nav-menu-toggler-icon">
									+
								</div>
							</div>
						</a>
					</li>
					<div id="our-way-mobile-collapser" class="mobile-show-hide collapse multi-collapse spacer-top-sm">
						<div class="row mobile-nav-sub-menu">
							<div class="col-12">
								<a class="nav-sub-link" href="#">Ιστοριά</a>
							</div>
						</div>
						<div class="row mobile-nav-sub-menu mobile-spacer-top-md">
							<div class="col-12">
								<a class="mobile-nav-sub-link" href="#">Φιλοσοφία</a>
							</div>
						</div>
						<div class="row mobile-nav-sub-menu mobile-spacer-top-md">
							<div class="col-12">
								<a class="mobile-nav-sub-link" href="#">Αξίες</a>
							</div>
						</div>
						<div class="row mobile-nav-sub-menu mobile-spacer-top-md">
							<div class="col-12">
								<a class="mobile-nav-sub-link" href="#">Κουλτούρα Τελειότητας</a>
							</div>
						</div>
						<div class="row mobile-nav-sub-menu mobile-spacer-top-md">
							<div class="col-12">
								<a class="mobile-nav-sub-link" href="#">Ποδια, ο καθημερινός μας σύμμαχος</a>
							</div>
						</div>
						<div class="row mobile-nav-sub-menu mobile-spacer-top-md mobile-spacer-bottom-md">
							<div class="col-12">
								<a class="mobile-nav-sub-link" href="#">Η τέχνη της φροντίδας των κάτω άκρων</a>
							</div>
						</div>
					</div>
					<li class="nav-item">
						<a class="nav-link desktop-show-hide" href="#" title="" itemdrop="url">Προιόντα</a>
						<a class="nav-link mobile-nav-collapser-btn mobile-show-hide" id="mobile-nav-our-way-btn" data-toggle="collapse" href="#products-mobile-collapser" role="button"
						aria-expanded="false" aria-controls="products-mobile-collapser">
							<div class="row">
								<div class="col-6 text-left">
									Προιόντα
								</div>
								<div class="col-6 text-right mobile-nav-menu-toggler-icon">
									+
								</div>
							</div>
						</a>
					</li>
					<div id="products-mobile-collapser" class="mobile-show-hide collapse multi-collapse spacer-top-sm">
						<div class="row mobile-nav-sub-menu">
							<div class="col-12">
								<a class="nav-sub-link" href="#">Προιόντα 1</a>
							</div>
						</div>
						
						<div class="row mobile-nav-sub-menu mobile-spacer-top-md">
							<div class="col-12">
								<a class="nav-sub-link" href="#">Προιόντα 2</a>
							</div>
						</div>
						
						<div class="row mobile-nav-sub-menu mobile-spacer-top-md mobile-spacer-bottom-md">
							<div class="col-12">
								<a class="nav-sub-link" href="#">Προιόντα 3</a>
							</div>
						</div>
					</div>
					<li class="nav-item">
						<a class="nav-link desktop-show-hide" href="#" title="" itemdrop="url">Yοur Podia Story</a>
						<a class="nav-link mobile-nav-collapser-btn mobile-show-hide" id="mobile-nav-story-btn" data-toggle="collapse" href="#story-mobile-collapser" role="button"
						aria-expanded="false" aria-controls="story-mobile-collapser">
							<div class="row">
								<div class="col-6 text-left">
									Yοur Podia Story
								</div>
								<div class="col-6 text-right mobile-nav-menu-toggler-icon">
									+
								</div>
							</div>
						</a>
					</li>
					<div id="story-mobile-collapser" class="mobile-show-hide collapse multi-collapse spacer-top-sm">
						<div class="row mobile-nav-sub-menu">
							<div class="col-12">
								<a class="nav-sub-link" href="#">Yοur Podia Story 1</a>
							</div>
						</div>
						
						<div class="row mobile-nav-sub-menu mobile-spacer-top-md">
							<div class="col-12">
								<a class="nav-sub-link" href="#">Yοur Podia Story 2</a>
							</div>
						</div>
						
						<div class="row mobile-nav-sub-menu mobile-spacer-top-md mobile-spacer-bottom-md">
							<div class="col-12">
								<a class="nav-sub-link" href="#">Yοur Podia Story 3</a>
							</div>
						</div>
					</div>
					<li class="nav-item search">
						<img src="/img/Search.svg" />
					</li>
					<div class="row mobile-nav-socials mobile-spacer-top-md mobile-spacer-bottom-md">
						<div class="col-2">
							<a href="#"><img class="img-fluid" src="/img/Facebook-Icon.svg" /></a>
						</div>
						
						<div class="col-2">
							<a href="#"><img class="img-fluid" src="/img/Instagram-Icon.svg" /></a>
						</div>
						
						<div class="col-2">
							<a href="#"><img class="img-fluid" src="/img/LinkedIn-Icon.svg" /></a>
						</div>
					</div>
				</ul>
			</div>
		</div>
	</nav>
</div>