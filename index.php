<?php
//Display settings: true to include, false to exclude
$displayNav = true;
$displayIntro = true;
$displayNeeds = true;
$displayMap = true;
$displayNewsletter = true;

//Meta tag settings
$metaDescription = 'Some meta description';
$metaKeywords    = 'Some, meta, keywords'; 
$pageTitle       = 'Podia';
$metaLanguage    = 'el';

include_once($_SERVER['DOCUMENT_ROOT'].'/head/head.php');
?>

<!-- Navigation -->
<?php
if($displayNav) {
	include_once($_SERVER['DOCUMENT_ROOT'].'/nav/nav.php');
}
?>

<!-- Introduction -->
<?php
if($displayIntro) {
	include_once($_SERVER['DOCUMENT_ROOT'].'/content/intro.php');
}
?>

<!-- Needs -->
<?php
if($displayNeeds) {
	include_once($_SERVER['DOCUMENT_ROOT'].'/content/needs.php');
}
?>

<!-- Map -->
<?php
if($displayMap) {
	include_once($_SERVER['DOCUMENT_ROOT'].'/content/map.php');
}
?>

<!-- Newsletter -->
<?php
if($displayNewsletter) {
	include_once($_SERVER['DOCUMENT_ROOT'].'/content/newsletter.php');
}
?>

<!-- Footer -->
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/footer/footer.php'); ?>