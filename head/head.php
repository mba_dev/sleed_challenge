<?php
session_start();
ob_start();

//sets the language
if(isset($_GET["lang"]))
{
	$lang = $_SESSION["language"] = $_GET["lang"];
}
else
{
	if(isset($_SESSION["language"]))
	{
		$lang = $_SESSION["language"];
	}
	else
	{
		$lang = "el";
	}
}

//sets the meta tags if not defined
if(!$metaDescription)
{
	$metaDescription='Some meta description.';
}
if(!$metaKeywords)
{
	$metaKeywords='some, meta, keywords'; 
}
if(!$pageTitle)
{
	$pageTitle='Podia';
}

if(!$metaLanguage)
{
	$metaLanguage='el';
}
?>
<!DOCTYPE html>
<html lang="<?php echo $metaLanguage; ?>">

<head>
	<meta charset="utf-8">
	<title><?php echo $pageTitle; ?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta content="<?php echo $metaKeywords; ?>" name="keywords">
	<meta content="<?php echo $metaDescription; ?>" name="description">

	<!-- Favicons -->
	<link href="/img/favicon.png" rel="icon">
	<link href="/img/apple-touch-icon.png" rel="apple-touch-icon">

	<!-- Bootstrap CSS File -->
	<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Libraries CSS Files -->
	<link href="/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="/lib/animate/animate.min.css" rel="stylesheet">
	<link href="/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
	<link href="/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

	<!-- Main Stylesheet File -->
	<link href="/css/style.css" rel="stylesheet">
	
	<!-- Custom Stylesheet File -->
	<link href="/css/custom-style.css" rel="stylesheet">
</head>
<body id="page-top">