<div id="intro-section" class="intro bg-image img-responsive" style="background-image: url(/img/slider-podia-1.png)">
	<div class="intro-content display-table">
		<div class="table-cell">
			<div class="container mobile-intro mobile-show-hide" itemscope itemtype="http://schema.org/Thing">
				<span class="mobile-intro-title">
					Στη ζωή που ονειρευόμαστε,
				</span>
				
				<span class="mobile-intro-sub-title">
					<br />Φτάνουμε με τα πόδια<br />
				</span>
				
				<span class=" fa fa-angle-down"></span>
			</div>
		</div>
	</div>
</div>