<section id="newsletter-section" class="spacer-bottom-sm">
	<div class="container">
		<div class="row text-left desktop-show-hide inner-space-top-md">			
			<div class="col-md-6 d-inline-block">
				<div class="d-inline-block">
					<img class="n-letter-icon img-fluid" src="/img/mail-icon.svg">
				</div>
				<div class="n-letter-desktop-text d-inline-block">
					Eισάγετε το e-mail σας για να λαμβάνετε τα νέα μας
				</div>
			</div>
			
			<div class="col-md-5 d-inline-block left-margin-lg">
				<div class="d-inline-block">
					<input class="n-letter-input" type="email" placeholder="email">
				</div>
				<div class="n-letter-btn-container d-inline-block">
					<button type="button" class="btn btn-dark n-letter-btn-submit">Εγγραφή<i class="n-letter-check fa fa-check"></i></button>
				</div>
			</div>
		</div>
		
		<div class="row text-center d-flex justify-content-center mobile-show-hide">
			<div class="col-md-3 mobile-spacer-top-xl">
				<div class="d-flex justify-content-center">
					<img class="n-letter-icon img-fluid" src="/img/mail-icon.svg">
				</div>
			</div>
			
			<div class="col-md-4 mobile-spacer-top-sm">
				<div class="d-flex justify-content-center">
					Eισάγετε το e-mail σας </br>
					για να λαμβάνετε τα νέα μας
				</div>
			</div>
			
			<div class="col-md-4 mobile-spacer-top-md">
				<input class="n-letter-input" type="email" placeholder="email">
			</div>
			
			<div class="col-md-4 mobile-spacer-top-md">
				<button type="button" class="btn btn-dark n-letter-btn-submit">Εγγραφή<i class="n-letter-check fa fa-check"></i></button>
			</div>
		</div>
	</div>
</section>