<section id="map-section" class="spacer-top-sm mobile-spacer-top-xxl">
	<div class="mobile-store-locator mobile-show-hide">
		<div class="row text-left">
			<div class="col-md-12 mobile-locator-headline mobile-spacer-top-xxl">
				Find your store
			</div>
		</div>
		
		<div class="row text-left">
			<div class="col-md-12 mobile-locator-sub-headline">
				Βρες το κοντινότερο φαρμακείο
			</div>
		</div>
		
		<div class="row mobile-spacer-top-md">
			<div class="col-md-12 mobile-locator-input-headline">
				NOMOΣ
			</div>
		</div>
		<div class="row mobile-spacer-top-md">
			<div class="col-md-12">
				<input class="store-locator-input" type="text">
			</div>
		</div>
		
		<div class="row mobile-spacer-top-md">
			<div class="col-md-12 mobile-locator-input-headline">
				ΠΟΛΗ
			</div>
		</div>
		<div class="row mobile-spacer-top-md">
			<div class="col-md-12">
				<input class="store-locator-input" type="text">
			</div>
		</div>
		
		<div class="row mobile-spacer-top-md">
			<div class="col-md-12 mobile-locator-input-headline">
				Τ.Κ.
			</div>
		</div>
		<div class="row mobile-spacer-top-md">
			<div class="col-md-12">
				<input class="store-locator-input" type="text">
			</div>
		</div>
		
		<div class="row mobile-spacer-top-xl">
			<div class="col-md-12">
				<button type="button" class="btn btn-dark map-btn">Αναζήτηση<i class="map-btn-search fa fa-search"></i></button>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12 desktop-show-hide">
			<img class="img-fluid" src="/img/Store-Locator.png" />
		</div>
		
		<div class="col-12 mobile-map mobile-show-hide mobile-spacer-top-xxl">
			<img class="img-fluid" src="/img/Mobile-Store-Locator.png" />
		</div>
	</div>
</section>