<section id="needs-section">
	<div class="container">
		<div class="row content-slider-headline">
			<div class="col-md spacer-top-sm spacer-bottom-sm mobile-spacer-top-xl mobile-spacer-bottom-md">
				Φροντίζουμε για τις δικές σου ανάγκες
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4 mobile-spacer-bottom-xxl">
				<div class="row">
					<div class="col-md-12 content-slider-box">
						<img class="img-fluid" src="/img/guy-on-bike.png" />
					</div>
				</div>
				
				<div class="row learn-more-border">
					<div class="col-md-12 text-center">
						Άθληση & υγιή αθλητικά πέλματα
						<a class="learn-more-link" href="#"><img class="img-responsive img-description img-float-right" src="/img/Learn-More-Icon.svg" /></a>
					</div>
				</div>
			</div>
			
			<div class="col-md-4 mobile-spacer-bottom-xxl">
				<div class="row">
					<div class="col-md-12 content-slider-box">
						<img class="img-fluid" src="/img/old-couple-with-bike.png" />
					</div>
				</div>
				
				<div class="row learn-more-border red-section-content">
					<div class="col-md-12 text-center description-slider-box">
						Προστασία από ειδικές παθήσεις
						<a href="#"><img class="img-responsive img-description img-float-right" src="/img/Learn-More-Icon-On-Hover.svg" /></a>
					</div>
				</div>
			</div>
			
			<div class="col-md-4 mobile-spacer-bottom-md">
				<div class="row">
					<div class="col-md-12 content-slider-box">
						<img class="img-fluid" src="/img/ballarina-on-toes.png" />
					</div>
				</div>
				
				<div class="row learn-more-border">
					<div class="col-md-12 text-center">
						Περιποίηση & υγεία στα νύχια
						<a class="learn-more-link" href="#"><img class="img-responsive img-description img-float-right" src="/img/Learn-More-Icon.svg" /></a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row d-flex justify-content-center spacer-top-sm">
			<div class="col-md-2">
				<div class="d-flex justify-content-center">
					<div class="carousel-active d-inline-block">
					</div>
					
					<div class="carousel-inactive d-inline-block">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>