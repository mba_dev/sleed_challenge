function mobileTap(){
	$('.mobile-tap-animation').removeClass('mobile-tap-effect-fade-out');
	$('.mobile-tap-animation').addClass('mobile-tap-effect-fade-in');
	
	setTimeout( function() {
		$('.mobile-tap-animation').removeClass('mobile-tap-effect-fade-in');
		$('.mobile-tap-animation').addClass('mobile-tap-effect-fade-out');
	}, 350);
}

$('#footer-menu-contact-btn').on('click', function() {
	var htmlContent = $(this).html()
	
	if(htmlContent == 'Eπικοινωνία +') {
		$('#footer-menu-contact-btn').html('Eπικοινωνία -');
	}
	else {
		$('#footer-menu-contact-btn').html('Eπικοινωνία +');
	}
});

$('#footer-menu-info-btn').on('click', function() {
	var htmlContent = $(this).html()
	
	if(htmlContent == 'Πληροφορίες +') {
		$('#footer-menu-info-btn').html('Πληροφορίες -');
	}
	else {
		$('#footer-menu-info-btn').html('Πληροφορίες +');
	}
});

$( document ).ready(function() {
    $('.learn-more-link').on('mouseover', function() {
		$(this).find('img').attr('src', '/img/Learn-More-Icon-On-Hover.svg');
	});
	
	$('.learn-more-link').mouseout(function() {
		$(this).find('img').attr('src', '/img/Learn-More-Icon.svg');
	});
	
	$('.mobile-nav-collapser-btn').click(function() {
		var currIcon = $(this).find('.mobile-nav-menu-toggler-icon').text();
		var iconPlus = '+';
		var iconMinus = '-';
		
		if( currIcon.indexOf(''+iconPlus+'') >= 0) {
			$(this).find('.mobile-nav-menu-toggler-icon').html(iconMinus)
		}
		else {
			$(this).find('.mobile-nav-menu-toggler-icon').html(iconPlus)
		}
	});
});